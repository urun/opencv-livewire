.PHONY: clean build format

CXX = g++
CXXFLAGS = -I$(IDIR) -std=c++20 -I/usr/include/opencv4/\
	-pedantic -Wall -Wextra -Wno-unused -D_GLIBCXX_ASSERTIONS\
	-fstack-clash-protection -fstack-protector -fexceptions
IDIR = ./include
SRCDIR = ./src

ODIR = ./obj

LIBS = -lopencv_core -lopencv_imgproc -lopencv_highgui -lopencv_objdetect -lopencv_imgcodecs

_DEPS =
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ = main.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))


$(ODIR)/%.o: $(SRCDIR)/%.cpp $(DEPS)
	$(CXX) -c -o $@ $< $(CXXFLAGS)

build: $(OBJ)
	mkdir -p $(ODIR)
	$(CXX) -o livewire $^ $(CXXFLAGS) -O3 $(LIBS)

build-debug: $(OBJ)
	mkdir -p $(ODIR)
	$(CXX) -o livewire $^ $(CXXFLAGS) -g -O0 $(LIBS)

clean:
	rm -rf $(ODIR)/*.o latex/ html/

format:
	clang-format $(SRCDIR)/* --style="{BasedOnStyle: llvm, IndentWidth: 4, AccessModifierOffset: -4, ColumnLimit: 100}" -i

doc:
	doxygen Doxyfile