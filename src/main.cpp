/**
    Live wire using OpenCV
    @file main.cpp
    @author Mikołaj Turski
    @version 1.0 2020-06-23
*/

#include <filesystem>
#include <limits.h>
#include <queue>
#include <unordered_set>
#include <vector>

#define CVUI_IMPLEMENTATION
#include "cvui.h"

using namespace cv;
using std::cout, std::cin, std::endl;
using std::find;
using std::remove;
using std::string;
using std::unordered_set;
using std::vector;
using std::filesystem::directory_iterator;

// Constants and types

const string MAIN_WINDOW = "livewire";
const string VISUAL_WINDOW = "visuals";
const string EDGES_WINDOW = "edges";

const int SEL_SIZE = 3;          // Point selection size
const int SEL_COLOR = 0xff0000;  // Red
const int LINE_COLOR = 0xffff00; // Yellow
const int PROG_COLOR = 0x00ffff; // Cyan

typedef struct vertex {
    int x;
    int y;
    uint32_t dist;
    vertex *prev;
} vertex;

struct greater_ver {
    bool operator()(vertex *a, vertex *b) const { return a->dist > b->dist; }
};

typedef std::priority_queue<vertex *, vector<vertex *>, greater_ver> pri_queue;

// Declarations

// Main program loop.
void main_loop();

/**
    Let the user choose the input image.
    @return file name string
*/
string choose_image();

/**
    Take input image and write image of edges to out.
    @param image Input image
    @param out Output image
*/
void compute_edges(Mat image, Mat &out);

/**
    Display last click on image and return click position.
    @param image Input image
    @return Click position
*/
Point display_click(Mat &image);

/**
    Setup dijkstra algorithm data structures.
    @param pixels Matrix of input image pixels
    @param visuals Visuals matrix
    @param vertices Matrix of visual image
    @param shortest Sorted queue of shortest total distances
    @param start Start point
*/
void setup_dijkstra(Mat &pixels, Mat &visuals, vector<vector<vertex>> &vertices,
                    pri_queue &shortest, vertex &start);

/**
    Run iterations of dijkstra algorithm with given data structures.
    @param pixels Matrix of input image pixels
    @param vertices Matrix of visual image
    @param visited 2D vector of all graph vertices
    @param shortest Sorted queue of shortest total distances
    @param end End point
    @param progress Vector of visited points to draw
    @param line Output algorithm calculated path
*/
bool dijkstra(Mat &pixels, vector<vector<vertex>> &vertices, unordered_set<vertex *> &visited,
              pri_queue &shortest, Point end, vector<Point> &progress, vector<Point> &line);

/**
    Return if ver is in visited.
    @param visited Set of visited vertices
    @param ver Input vertex
*/
bool is_not_visited(unordered_set<vertex *> &visited, vertex ver);

/**
    Update neighbours of vertex.
    @param pixels Input image matrix
    @param vertices 2D vector of all vertices
    @param visited Set of visited vertices
    @param shortest Sorted queue of shortest total distances
    @param ver Vertex to calculate its neighbours
*/
void calculate_neighbours(Mat &pixels, vector<vector<vertex>> &vertices,
                          unordered_set<vertex *> &visited, pri_queue &shortest, vertex &ver);

/**
    Update distance of neighbour of current vertex with brightness.
    @param shortest Sorted queue of shortest total distances
    @param neigh Neighbour vertex
    @param current Current vertex
    @param brightness Current vertex brightness value
*/
void update_dist(pri_queue &shortest, vertex &neigh, vertex &current, uint brightness);

/**
    Draw points from line on the image.
    @param image Output image
    @param line Input line
*/
void draw_line(Mat &image, vector<Point> &line);

// Implementations

int main() {
    main_loop();
    return 0;
}

void main_loop() {
    Mat edges, visuals;
    vector<Point> line, progress;

    string img_file = choose_image();
    if (img_file == "") {
        cout << "No .jpg files in current directory." << endl;
        return;
    }

    cvui::init(MAIN_WINDOW);
    Mat image = imread(img_file);
    compute_edges(image, edges);
    cvui::imshow(MAIN_WINDOW, image);
    cvui::imshow(EDGES_WINDOW, edges);
    cvui::imshow(VISUAL_WINDOW, edges);

    vector<vector<vertex>> vertices(image.size().width, vector<vertex>(image.size().height));
    unordered_set<vertex *> visited;
    pri_queue shortest;
    vertex start;

    Point pos, prev_pos;
    prev_pos.x = -1; // signify no previous position
    bool calculating = false;
    bool clicked = false;
    while (1) {
        clicked = cvui::mouse(cvui::CLICK);
        if (clicked) {
            if (!calculating) {
                pos = display_click(image);
            }
            bool prev_exists = prev_pos.x != -1;
            if (!calculating && prev_exists) {
                start = vertex{prev_pos.x, prev_pos.y, 0, &start};
                setup_dijkstra(edges, visuals, vertices, shortest, start);
                calculating = true;
            }
            prev_pos = pos;
        }

        if (calculating) {
            calculating = dijkstra(edges, vertices, visited, shortest, pos, progress, line);
            for (Point p : progress) {
                cvui::rect(visuals, p.x, p.y, 2, 2, PROG_COLOR);
            }
            imshow(VISUAL_WINDOW, visuals);
            progress.clear();
        } else {
            draw_line(image, line);
            line.clear();
        }
        if (waitKey(20) == 27) {
            break;
        }
    }

    destroyAllWindows();
}

string choose_image() {
    auto path = std::filesystem::current_path();
    string ending = ".jpg";
    vector<string> files;
    for (const auto &entry : directory_iterator(path)) {
        string file = entry.path().string();
        if (file.compare(file.length() - ending.length(), ending.length(), ending) == 0) {
            files.push_back(file);
        }
    }
    if (files.size() == 0) {
        return "";
    }

    bool chosen = false;
    string ans;
    while (!chosen) {
        cout << "Choose a file:" << endl;
        for (int i = 0; i < files.size(); i++) {
            cout << i << ". " << files.at(i) << endl;
        }
        cin >> ans;
        if (!ans.empty() && std::all_of(ans.begin(), ans.end(), isdigit)) {
            int ans_n = stoi(ans);
            if (ans_n < files.size()) {
                path = files.at(ans_n);
                chosen = true;
            }
        }
    }
    return path;
}

void compute_edges(Mat image, Mat &out) {
    Mat gray, sobelx, sobely, abs_sobelx, abs_sobely, edges;
    cvtColor(image, gray, COLOR_BGR2GRAY);

    GaussianBlur(gray, gray, Point(3, 3), 0);

    Sobel(gray, sobelx, CV_64F, 1, 0, 3);
    Sobel(gray, sobely, CV_64F, 0, 1, 3);

    convertScaleAbs(sobelx, abs_sobelx);
    convertScaleAbs(sobely, abs_sobely);
    addWeighted(abs_sobelx, 0.5, abs_sobely, 0.5, 0, edges);
    threshold(edges, out, 50, 255, THRESH_TOZERO);
}

Point display_click(Mat &image) {
    Point pos = cvui::mouse(MAIN_WINDOW);
    int s = SEL_SIZE;
    cvui::rect(image, pos.x - s / 2, pos.y - s / 2, s, s, SEL_COLOR);
    cvui::imshow(MAIN_WINDOW, image);
    return pos;
}

void setup_dijkstra(Mat &pixels, Mat &visuals, vector<vector<vertex>> &vertices,
                    pri_queue &shortest, vertex &start) {
    while (!shortest.empty()) {
        shortest.pop();
    }
    pixels.copyTo(visuals);
    cvtColor(visuals, visuals, COLOR_GRAY2BGR);
    for (int x = 0; x < pixels.size().width; x++) {
        for (int y = 0; y < pixels.size().height; y++) {
            vertices.at(x).at(y).x = x;
            vertices.at(x).at(y).y = y;
            vertices.at(x).at(y).dist = UINT32_MAX;
            vertices.at(x).at(y).prev = &vertices.at(x).at(y);
        }
    }
    shortest.push(&start);
}

bool dijkstra(Mat &pixels, vector<vector<vertex>> &vertices, unordered_set<vertex *> &visited,
              pri_queue &shortest, Point end, vector<Point> &progress, vector<Point> &line) {
    vertex *current;

    bool done = false;
    for (int i = 0; i < 10; i++) {
        current = shortest.top();
        shortest.pop();
        if (is_not_visited(visited, *current)) {
            visited.insert(current);
            calculate_neighbours(pixels, vertices, visited, shortest, *current);
        }
        progress.push_back(Point(current->x, current->y));
        if (current->x == end.x && current->y == end.y) {
            while (current->prev != current) {
                line.push_back(Point(current->x, current->y));
                current = current->prev;
            }
            return false;
        }
    }
    return true;
}

bool is_not_visited(unordered_set<vertex *> &visited, vertex ver) {
    return visited.find(&ver) == visited.end();
}

void calculate_neighbours(Mat &pixels, vector<vector<vertex>> &vertices,
                          unordered_set<vertex *> &visited, pri_queue &shortest, vertex &ver) {
    int x = ver.x;
    int y = ver.y;
    // top left
    if (x > 0 && y > 0) {
        vertex &neigh = vertices.at(x - 1).at(y - 1);
        if (is_not_visited(visited, neigh)) {
            update_dist(shortest, neigh, ver, pixels.at<uchar>(y, x + 1));
        }
    }
    // top
    if (y > 0) {
        vertex &neigh = vertices.at(x).at(y - 1);
        if (is_not_visited(visited, neigh)) {
            update_dist(shortest, neigh, ver, pixels.at<uchar>(y, x + 1));
        }
    }
    // top right
    if (x + 1 < pixels.size().width && y > 0) {
        vertex &neigh = vertices.at(x + 1).at(y - 1);
        if (is_not_visited(visited, neigh)) {
            update_dist(shortest, neigh, ver, pixels.at<uchar>(y, x + 1));
        }
    }
    // right
    if (x + 1 < pixels.size().width) {
        vertex &neigh = vertices.at(x + 1).at(y);
        if (is_not_visited(visited, neigh)) {
            update_dist(shortest, neigh, ver, pixels.at<uchar>(y, x + 1));
        }
    }
    // bot right
    if (x + 1 < pixels.size().width && y + 1 < pixels.size().height) {
        vertex &neigh = vertices.at(x + 1).at(y + 1);
        if (is_not_visited(visited, neigh)) {
            update_dist(shortest, neigh, ver, pixels.at<uchar>(y + 1, x + 1));
        }
    }
    // bot
    if (y + 1 < pixels.size().height) {
        vertex &neigh = vertices.at(x).at(y + 1);
        if (is_not_visited(visited, neigh)) {
            update_dist(shortest, neigh, ver, pixels.at<uchar>(y + 1, x));
        }
    }
    // bot left
    if (x > 0 && y + 1 < pixels.size().height) {
        vertex &neigh = vertices.at(x - 1).at(y + 1);
        if (is_not_visited(visited, neigh)) {
            update_dist(shortest, neigh, ver, pixels.at<uchar>(y + 1, x - 1));
        }
    }
    // left
    if (x > 0) {
        vertex &neigh = vertices.at(x - 1).at(y);
        if (is_not_visited(visited, neigh)) {
            update_dist(shortest, neigh, ver, pixels.at<uchar>(y, x - 1));
        }
    }
}

void update_dist(pri_queue &shortest, vertex &neigh, vertex &current, uint brightness) {
    uint32_t dist = 255 - brightness;
    if (dist + current.dist < neigh.dist) {
        neigh.dist = dist + current.dist;
        neigh.prev = &current;
        shortest.push(&neigh);
    }
}

void draw_line(Mat &image, vector<Point> &line) {
    for (int i = 0; i < line.size(); i++) {
        Point p = line.at(i);
        cvui::rect(image, p.x - 1, p.y - 1, 2, 2, LINE_COLOR);
    }
    cvui::imshow(MAIN_WINDOW, image);
}