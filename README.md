# OpenCV Livewire
Livewire implementation using modern C++, OpenCV and [cvui](https://dovyski.github.io/cvui/).

## Compile
`git clone https://gitlab.com/urun/opencv-livewire.git`

`cd opencv-livewire`

`make`


## Run
`./livewire`